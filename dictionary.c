/*
    Tiffany Andersen
    CSCI 46
    11/10/21

    This program loads a file into memory by creating an array of pointers to each word from the file.
    It then searches the array and returns the word searched for by the user if it is found.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open dictionary");
	    exit(1);
	}
	
	// Allocate memory for an array of strings (arr).
    char **words = malloc(50 * sizeof(char *));

	// Read the dictionary line by line.
    int capacity = 10;
    char line[100];
    
    // The size should be the number of entries in the array.
	*size = 0;

    //make an integer to count words in dictionary file
    //tried to just use the int* size but had trouble.
    int sizetest = 0;
    while(fgets(line, 100, in) != NULL)
    {   
        //Trim newline
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

        //put each word from huge array into a malloced space
        // Allocate memory for the string (str).
        char *word = malloc(strlen(line) + 1);

        // Copy each line into the string (use strcpy).
        strcpy(word, line); 

        // Attach the string to the large array (assignment =).
        words[*size] = word;
        
        //Test
        //printf("%s\n", words[*size]);
        
        // Expand array if necessary (realloc).
        if (*size == capacity)
        {
            //Make array bigger
            capacity += 10;
            //assign the bigger array back into words
            words = realloc(words, capacity * sizeof(char *));
        }

        //increment sizetest and copy count into int pointer to return
        sizetest++;
        *size = sizetest;
    } 	
    
    //test size
    //printf("Size of file is %d words\n", *size);

    // Return pointer to the array of strings.
    if (words != NULL)
        return words;
    return NULL;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}